#include "adder.hpp"
#include <rip/logger.hpp>

extern std::atomic<int> solution1;

namespace es_test
{

    Adder::Adder(const nlohmann::json& config, std::shared_ptr<rip::EventSystem> es, 
            std::string id,
            std::shared_ptr<std::unordered_map<std::string, 
                std::shared_ptr<rip::RobotComponent> > > comps)
        : rip::Routine(config, es, id, comps)
    {
        m_base = config["base"];
        m_add = config["second"];
    }

    void Adder::start(std::vector<std::any> data)
    {
        rip::Logger::info("Adder: Starting.");
        if (data.size() == 0)
        {
            run();
        }
        else
        {
            m_base = rip::cast_param<int>(data[0]);
            run();
        }
    }

    void Adder::stop(std::vector<std::any> data)
    {
        if (data.size() > 0 && m_id == "adr1")
        {
            solution1 = rip::cast_param<int>(data[0]);
        }
        rip::Logger::info("Adder: Stopping");
        sendMessage("added", data);
        m_esystem->removeRoutine(m_id);
    }

    // The routines will not make subscriptions in v1 of this test.
    void Adder::handle_subscription(std::string handle, std::string func_id, 
            std::string routine_id)
    {
    }

    void Adder::run()
    {
        rip::Logger::info("Adder: Running");
        std::vector<std::any> data;
        data.push_back(m_base + m_add);
        stop(data);
    }

}
