#include <signal.h>

#include <chrono>
#include <rip/logger.hpp>
#include <rip/exception_base.hpp>
#include <rip/robot.hpp>
#include <thread>

#include "adder.hpp"
#include "divider.hpp"
#include "multiplier.hpp"
#include "subtractor.hpp"

#define LOG_DEBUG rip::Logger::debug
#define LOG_INFO rip::Logger::info
#define LOG_ERR rip::Logger::error

extern std::atomic<int> solution0;
extern std::atomic<int> solution1;
extern std::atomic<int> solution2;
extern std::atomic<int> solution3;

/*void signalHandler(int signum)
{
    LOG_DEBUG("Caught Ctrl-C. Stoping the robot.");
    robot->stop();
}*/

using namespace es_test;

int main(int argc, char** argv)
{
    std::shared_ptr< rip::Robot > robot = nullptr;
    bool diag = argc > 1 && std::string(argv[1]) == "--diag";

    std::shared_ptr<rip::RoutineFactory> rfact;
    rfact->registerRoutine<Adder>("Adder");
    rfact->registerRoutine<Divider>("Divider");
    rfact->registerRoutine<Multiplier>("Multiplier");
    rfact->registerRoutine<Subtractor>("Subtractor");

    std::shared_ptr<rip::ComponentFactory> cfact;

    robot     = std::make_shared<rip::Robot>(std::string("es_test"), std::string("./config.json"),
            std::string("multiplied:mpl0"), 4, rfact, cfact);
    robot->load();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    //signal(SIGINT, signalHandler);
    try
    {
        if(diag)
        {
            LOG_INFO("Diag mode starts...");
            std::this_thread::sleep_for(std::chrono::milliseconds(500));

            // XXX do your stuff here

            LOG_INFO("Diag exited.");
        }
        else
        {  // robot not in diag mode
            LOG_INFO("robot->start()");
            robot->start();
        }
    }
    catch(rip::ExceptionBase e)
    {
        LOG_ERR("Caught an exception from RIP.");
        LOG_ERR(e.what());
        robot->stop();
        throw e;
        return 1;
    }

    LOG_INFO("Solution0 is {}", solution0);
    LOG_INFO("Solution1 is {}", solution1);
    LOG_INFO("Solution2 is {}", solution2);
    LOG_INFO("Solution3 is {}", solution3);

    LOG_INFO("End of program. Stopping robot...");

    LOG_INFO("Robot stopped.");

    return 0;
}
