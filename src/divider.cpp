#include "divider.hpp"

extern std::atomic<int> solution3;

namespace es_test
{

    Divider::Divider(const nlohmann::json& config, std::shared_ptr<rip::EventSystem> es, 
            std::string id,
            std::shared_ptr<std::unordered_map<std::string, 
                std::shared_ptr<rip::RobotComponent> > > comps)
        : rip::Routine(config, es, id, comps)
    {
        m_base = config["base"];
        m_div = config["second"];
    }

    void Divider::start(std::vector<std::any> data)
    {
        if (data.size() == 0)
        {
            run();
        }
        else
        {
            m_base = rip::cast_param<int>(data[0]);
            run();
        }
    }

    void Divider::stop(std::vector<std::any> data)
    {
        if (data.size() > 0 && m_id == "div3")
        {
            solution3 = rip::cast_param<int>(data[0]);
        }
        sendMessage("divided", data);
        m_esystem->removeRoutine(m_id);
    }

    // The routines will not make subscriptions in v1 of this test.
    void Divider::handle_subscription(std::string handle, std::string func_id, 
            std::string routine_id)
    {
    }

    void Divider::run()
    {
        std::vector<std::any> data;
        data.push_back(m_base / m_div);
        stop(data);
    }

}
