#include "multiplier.hpp"

extern std::atomic<int> solution0;

namespace es_test
{

    Multiplier::Multiplier(const nlohmann::json& config, std::shared_ptr<rip::EventSystem> es, 
            std::string id, 
            std::shared_ptr<std::unordered_map<std::string, 
                std::shared_ptr<rip::RobotComponent> > > comps)
        : rip::Routine(config, es, id, comps)
    {
        m_base = config["base"];
        m_mul = config["second"];
    }

    void Multiplier::start(std::vector<std::any> data)
    {
        if (data.size() == 0)
        {
            run();
        }
        else
        {
            m_base = rip::cast_param<int>(data[0]);
            run();
        }
    }

    void Multiplier::stop(std::vector<std::any> data)
    {
        if (data.size() > 0 && m_id == "mpl0")
        {
            solution0 = rip::cast_param<int>(data[0]);
        }
        sendMessage("multiplied", data);
        m_esystem->removeRoutine(m_id);
    }

    // The routines will not make subscriptions in v1 of this test.
    void Multiplier::handle_subscription(std::string handle, std::string func_id, 
            std::string routine_id)
    {
    }

    void Multiplier::run()
    {
        std::vector<std::any> data;
        data.push_back(m_base * m_mul);
        stop(data);
    }

}
