# RIP EventSystem Test

This repo is a __software-only__ test of RIP's event system. It should not be used as the starting point for any actual robot, although it can provide a decent example of how to make your own Routines and how to use rip::Robot.

Currently the code should solve the following 4 simple math problems in parallel:
```
0: (((2 + 4) / 2) - 1) * 2 = 4
1: (((5 * 2) - 4) / 2) + 3 = 6
2: (((10 / 2) + 2) * 3) - 1 = 20
3: (((5 - 3) * 5) + 10) / 4 = 5
```
