cmake_minimum_required(VERSION 3.1)
project(event_system_test)

add_subdirectory(scripts)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/x${PROJECT_NAME}lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/x${PROJECT_NAME}lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/x${PROJECT_NAME}bin)
set(CMAKE_BINARY_DIR ${CMAKE_BINARY_DIR}/x${PROJECT_NAME}bin)
set(CMAKE_CURRENT_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/x${PROJECT_NAME}bin)

if(ENABLE_TESTING)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -O0 -fprofile-arcs -ftest-coverage")
  set(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} -lgcov --coverage")
  set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -lgcov --coverage")
endif()

# LIST(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMakeModules)
add_subdirectory(rip)

add_subdirectory(components)
add_subdirectory(routines)

# include(FileOutputs)
file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/config.json" DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")
# file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/rip/appendages/json" DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/appendages/")
# file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/rip/appendages/xml" DESTINATION "${CMAKE_CURRENT_BINARY_DIR}/appendages/")
# file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/rip/arduino_gen/code_template.txt" DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")

# Get all .cpp files except for main.cpp
file(GLOB_RECURSE ${PROJECT_NAME}_HEADERS "include/*.hpp")
file(GLOB_RECURSE ${PROJECT_NAME}_SOURCES "src/*.cpp")
list(REMOVE_ITEM ${PROJECT_NAME}_SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp")

# Create the library, set C++ version, link external libraries, and set include dir
add_library(lib_${PROJECT_NAME} ${${PROJECT_NAME}_SOURCES} ${${PROJECT_NAME}_HEADERS} ${CONFIGS})
set_property(TARGET lib_${PROJECT_NAME} PROPERTY CXX_STANDARD 17)
target_link_libraries(lib_${PROJECT_NAME} rip fmt)
target_include_directories(lib_${PROJECT_NAME} PUBLIC include)

add_executable(${PROJECT_NAME} "src/main.cpp")
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 17)
target_link_libraries(${PROJECT_NAME} lib_${PROJECT_NAME})
