#ifndef DIVIDER_HPP
#define DIVIDER_HPP

#include <rip/routine.hpp>

std::atomic<int> solution3;

namespace es_test
{

    class Divider : public rip::Routine
    {
        public:

            Divider(const nlohmann::json& config, std::shared_ptr<rip::EventSystem> es, 
                    std::string id, 
                    std::shared_ptr<std::unordered_map<std::string, 
                        std::shared_ptr<rip::RobotComponent> > > comps);

            void start(std::vector<std::any> data = {}) override;
            
            virtual void stop(std::vector<std::any> data = {}) override;

        protected:
            
            void accessComponents(std::shared_ptr<std::unordered_map<std::string, 
                    std::shared_ptr<rip::RobotComponent> > > comps) override
            {
            }

            virtual void handle_subscription(std::string handle, std::string func_id,
                    std::string routine_id="") override;

            virtual void run() override;

        private:

            int m_base;
            int m_div;
    };

}

#endif
